import java.awt.*;
import java.util.ArrayList;

public class Main {
    public static char userInput = 'N';

    public static void main(String[] args) {
        StoryNode curNode = initializeStoryNodes();
        drawStoryCard(curNode);
        while (true) {
            if (StdDraw.hasNextKeyTyped()) {
                userInput = StdDraw.nextKeyTyped();
                if (userInput == '1') {
                    if (curNode.choices.get(0).equals("game over")) {
                        break;
                    }
                    curNode = curNode.children.get(0);
                    drawStoryCard(curNode);
                } else if (userInput == '2' && curNode.choices.size() > 1) {
                    curNode = curNode.children.get(1);
                    drawStoryCard(curNode);
                } else if (userInput == '3' && curNode.choices.size() > 2) {
                    curNode = curNode.children.get(2);
                    drawStoryCard(curNode);
                }
            }
        }
        System.exit(0);
    }

    /**
     * This method draws the actual screen that tells the tale of Odysseus and presents them with options.
     *
     * @param curNode is the story node that this method draws the story card for.
     */
    private static void drawStoryCard(StoryNode curNode) {
        StdDraw.setCanvasSize(1000, 512);
        StdDraw.clear(Color.BLACK);
        StdDraw.setPenColor(Color.WHITE);
        Font regFont = new Font("Gill Sans", Font.PLAIN, 16);
        StdDraw.setFont(regFont);
        for (int i = 0; i < curNode.text.length; i++) {
            StdDraw.text(0.5, 0.9 - (0.05 * i), curNode.text[i]);
        }
        for (int i = 0; i < curNode.choices.size(); i++) {
            StdDraw.text(0.5, 0.3 - (0.1 * i), Integer.toString(i + 1) + ") " + curNode.choices.get(i));
        }
    }

    /**
     * This method creates the entire story in the form of a node tree. The node tree begins with the start of the
     * Odyssey, explaining the background and such. As the story continues, there are certain points in the story where
     * the player can make certain choices in order to impact the story.
     *
     * @return The beginning (original) story node, with the entire story tree initialized.
     */
    private static StoryNode initializeStoryNodes() {
        String[] titleCard = {"JOURNEY BACK TO ITHACA", "A game made by Matthew Kwon for the CLASSICS 28 myth retelling project",
                "Inspired by Homer's Odyssey and Odysseus' journey to return to his homeland, Ithaca."};
        ArrayList<String> titleChoices = new ArrayList<>();
        titleChoices.add("begin game");
        StoryNode title = new StoryNode(titleCard, titleChoices);

        // initializing the beginning
        String[] beginningText = {"It is 10 years after the Trojan War. Greek, triumphantly victorious",
                "after the siege on Troy, has sent all of its heroes home. All of its heroes except one: you, Odysseus,",
                "whose misfortune brought you to the island Ogygia. The goddess Calypso is entranced by you, and is going",
                "so far as to offer you immortality in exchange for your love back. Although it sounds like a blessing,",
                "there is nothing you want more than to return home to Ithaca, where your family patiently waits. Little",
                "do you know, Ithaca is being occupied by suitors who wish to court your wife and take over your kingdom.",
                "You must get back to Ithaca as soon as possible, to reestablish your rightful control of the kingdom and",
                "see your family again."};
        ArrayList<String> choices = new ArrayList<>();
        choices.add("continue");
        StoryNode beginning = new StoryNode(beginningText, choices);
        title.children.add(beginning);

        // initializing the events on Calypso's island
        String[] calypsoCallText = {"You must find a way to escape off this island and get back home. As enticing as Calypso is, you",
                "have a duty to your kingdom, and your family awaits. The problem is, how? As you ponder this question, you",
                "hear Calypso call for your name."};
        ArrayList<String> calypsoCallChoices = new ArrayList<>();
        calypsoCallChoices.add("ignore her call and look around");
        calypsoCallChoices.add("go to her");
        StoryNode calypsoCall = new StoryNode(calypsoCallText, calypsoCallChoices);
        beginning.children.add(calypsoCall);

        String[] ignoreCallText = {"You decide to ignore Calypso's call in order to explore your options. You walk away from",
                "where the voice is coming from and look around to see what possible ways you have off of this island. To",
                "the left, you see the the shoreline with the waves crashing against the rocks. To the right, there is a",
                "cavernous rock structure that might have something in it. Out of the corner of your eye, you also see",
                "something glistening..."};
        ArrayList<String> ignoreCallChoices = new ArrayList<>();
        ignoreCallChoices.add("explore the shoreline");
        ignoreCallChoices.add("check out the caverns");
        ignoreCallChoices.add("see what the glistening object is");
        StoryNode ignoreCall = new StoryNode(ignoreCallText, ignoreCallChoices);
        calypsoCall.children.add(ignoreCall);

        String[] goToHerText = {"You decide that it's probably better to listen to what a god has to say, especially when",
                "she might be the only way off the island. You follow the voice and call back to her to let her know",
                "where you are, and you both eventually find each other. She seems to have a serious, melancholic look",
                "on her face."};
        ArrayList<String> goToHerChoices = new ArrayList<>();
        goToHerChoices.add("ask her why she called you");
        StoryNode goToHer = new StoryNode(goToHerText, goToHerChoices);
        calypsoCall.children.add(goToHer);


        String[] exploreShorelineText = {"The shoreline seems to be the best choice, especially considering that you will",
                "most likely need it to get off of this island. You walk over to the shoreline, muffling your footsteps",
                "so Calypso won't hear you moving around. The coarse sand makes footsteps louder than they should be, a",
                "lesson you've already learned from trying to find ways to escape before. You reach the shoreline, but",
                "there's nothing here besides the rocks, the crashing waves, and the place where you grieve over your",
                "inability to return to Ithaca. Calypso's voice grows louder. It's probably best to just see what she",
                "has to say at this point."};
        ArrayList<String> exploreShorelineChoices = new ArrayList<>();
        exploreShorelineChoices.add("return to Calypso");
        StoryNode exploreShoreline = new StoryNode(exploreShorelineText, exploreShorelineChoices);
        ignoreCall.children.add(exploreShoreline);
        exploreShoreline.children.add(goToHer);

        String[] cavernsText = {"The caverns have been an area Calypso has told you multiple times to avoid, due to the",
                "dangerous pitfalls that await. You can't help but think that her reasoning is just a guise: a way for",
                "her to hide something that might get you off the island. You step near the entrance of the caverns",
                "before you hear Calypso's voice extremely close, calling for your name, this time, louder. There are",
                "sure to be consequences if you continue to ignore her, but you can't help but think about the possibility",
                "that these caverns could be your way out."};
        ArrayList<String> cavernsChoices = new ArrayList<String>();
        cavernsChoices.add("go back to Calypso");
        cavernsChoices.add("venture the caverns");
        StoryNode caverns = new StoryNode(cavernsText, cavernsChoices);
        ignoreCall.children.add(caverns);
        caverns.children.add(goToHer);

        String[] glisteningObjectText = {"The shine of the object catches your eye, and you decide that you wouldn't get",
                "much time to explore the nearby areas with Calypso looking for you anyway. You head directly over to",
                "the glistening object on the ground, and you find that it's buried partially underneath the sand.",
                "Uncovering it, you see that it is some sort of coin, most likely used as an offering to the gods",
                "before eventually being swept away to this island. Turning the coin over, you see that it has a",
                "symbol of a trident: representative of Poseidon. A chill runs down your spine as you see it. You",
                "know that Athena, the god giving you favor, is not in the best of terms with Poseidon. It is most",
                "wise to leave the coin behind, but perhaps it will eventually hold some value while you are traversing",
                "the sea to get back home."};
        ArrayList<String> glisteningObjectChoices = new ArrayList<>();
        glisteningObjectChoices.add("pick up the coin");
        glisteningObjectChoices.add("choose to leave it behind");
        StoryNode glisteningObject = new StoryNode(glisteningObjectText, glisteningObjectChoices);
        ignoreCall.children.add(glisteningObject);

        String[] whyCallText = {"You ask her what she needs. With a heavy heart, she explains that the supreme god Zeus",
                "has ordered her to help you get off the island and back to your family. You are ecstatic, but you do not",
                "show it, because you can tell that Calypso's emotions are as down-trod as yours are upbeat. The next",
                "day, she helps you build a new boat and provides you with some food, water, and other necessities for",
                "a safe journey back. As you set sail, you look back one more time to see her face wrenched with sorrow.",
                "You cannot help but sympathize for her, but you steel yourself and come face with the sea. This is where",
                "the true journey begins."};
        ArrayList<String> whyCallChoices = new ArrayList<>();
        whyCallChoices.add("journey onwards");
        StoryNode whyCall = new StoryNode(whyCallText, whyCallChoices);
        goToHer.children.add(whyCall);

        String[] ventureCavernsText = {"The risk of the caverns is something you are willing to take in order to get back",
                "to your family and your kingdom. As Calypso calls you, you rush into the caverns to look around before",
                "she can find you. It's dark, and a heavy pungency makes it difficult to breath the air in the caverns.",
                "Maybe it wasn't the best idea to come in here after all, especially because your legs are starting to",
                "to give out due to the lack of clean air in the caverns. There must be something in here making it hard",
                "to breath, and you begin to stumble around..."};
        ArrayList<String> ventureCavernsChoices = new ArrayList<>();
        ventureCavernsChoices.add("hope for the best as you keep stumbling");
        StoryNode ventureCaverns = new StoryNode(ventureCavernsText, ventureCavernsChoices);
        caverns.children.add(ventureCaverns);

        String[] stumblingText = {"Suddenly, you hear Calypso's voice directly behind you! You jump forward out of fright,",
                "and trip over a jagged crack in the floor, launching you over a great chasm. You scream as you fall,",
                "but nobody is around to hear the last sounds to come out of the mouth of the once great Odysseus. You",
                "die a tragic death, with your spirit wandering around in the afterlife, powerless to stop the suitors",
                "from taking over your kingdom."};
        ArrayList<String> stumblingChoices = new ArrayList<>();
        stumblingChoices.add("game over");
        StoryNode stumbling = new StoryNode(stumblingText, stumblingChoices);
        ventureCaverns.children.add(stumbling);

        String[] pickUpCoinText = {"You pick up the coin despite being allied to Athena, a grave mistake. You suddenly feel",
                "a piercing sight as your vision blacks out. An image of Athena in her full wrath twists your mind. You",
                "have angered your sole ally in your struggle to the journey back home, and the consequence is dear.",
                "You begin to seize on the ground as Athena takes all wisdom from you, reducing you to a fraction of",
                "the once admirable figure that you were. You forget how to stand. You fall. The waves carry you away",
                "as you forget how to swim, and eventually, forget how to breath. You drown in the very sea that you",
                "hoped picking up the coin would give you favors for."};
        ArrayList<String> pickUpCoinChoices = new ArrayList<>();
        pickUpCoinChoices.add("game over");
        StoryNode pickUpCoin = new StoryNode(pickUpCoinText, pickUpCoinChoices);
        glisteningObject.children.add(pickUpCoin);
        glisteningObject.children.add(goToHer);

        // initializing the events that happen at sea
        String[] leaveIslandText = {"Leaving Calypso, you sail off to the unknown future. The ocean remains calm, at least",
                "around the island. You check your provisions given to you by Calypso, and they are plentiful, so",
                "there's nothing to worry about there. With the calm breeze, you drift off to sleep, something that",
                "you will definitely need in the upcoming days."};
        ArrayList<String> leaveIslandChoices = new ArrayList<>();
        leaveIslandChoices.add("continue");
        StoryNode leaveIsland = new StoryNode(leaveIslandText, leaveIslandChoices);
        whyCall.children.add(leaveIsland);

        String[] leaveIslandContinuedText = {"Several days later, you see a land mass ahead. Upon getting closer, you see",
                "that it's Scheria, the land of the Phaeacians. You remember that Athena told you that you were destined",
                "to come here, but before you can rejoice your success, the waves begin to stir. Somewhat unnaturally.",
                "Actually, really unnaturally! The tides move as if they want to devour you. This must be the work of",
                "an angered Poseidon!"};
        ArrayList<String> leaveIslandContinuedChoices = new ArrayList<>();
        leaveIslandContinuedChoices.add("struggle to survive");
        StoryNode leaveIslandContinued = new StoryNode(leaveIslandContinuedText, leaveIslandContinuedChoices);
        leaveIsland.children.add(leaveIslandContinued);

        String[] struggleText = {"The waters seem to roar at you as they prepare to devour you and your ship. At this rate,",
                "you see no way that your ship will survive, but perhaps it doesn't need to. The island is within sight,",
                "and although you are weakened, you are still the great Greek hero Odysseus, and you can probably swim",
                "your way over to the shore with enough effort. The greatest problem is that the current is not in your",
                "favor, and with Poseidon influencing the waters, you may very well die for leaving the ship."};
        ArrayList<String> struggleChoices = new ArrayList<>();
        struggleChoices.add("take the risk and swim");
        struggleChoices.add("stay on the ship and hope");
        StoryNode struggle = new StoryNode(struggleText, struggleChoices);
        leaveIslandContinued.children.add(struggle);

        String[] takeRiskAndSwimText = {"You decide that there are no other options than to take the risk and try to swim to",
                "shore. As soon as you get off, you realize that your decision was a big mistake. You are easily tossed",
                "around by the enraged sea and you are thrown across rocks, knocking the breath out of your lungs. You",
                "can barely breath, and the little air you have is being taken away by the mad struggle of survival.",
                "Getting to shore is no longer the priority: it's surviving."};
        ArrayList<String> takeRiskAndSwimChoices = new ArrayList<>();
        takeRiskAndSwimChoices.add("try to get back to the ship");
        takeRiskAndSwimChoices.add("aim for the shore anyway");
        StoryNode takeRiskAndSwim = new StoryNode(takeRiskAndSwimText, takeRiskAndSwimChoices);
        struggle.children.add(takeRiskAndSwim);

        String[] getBackToShipText = {"There's no way you are making it to that shore in this condition. You start to head",
                "back, but you turn around directly into large whirlpool. Already exhausted from the struggle, you are",
                "not able to get out of the clutches of the whirlpool as it tosses you around, before finally sucking",
                "you underwater. Eventually, you resurface, but your ship is nowhere to be found. The shoreline is even",
                "farther than it was before, and you have no strength left in your body. Slowly, quietly, you sink into",
                "Poseidon's watery graves."};
        ArrayList<String> getBackToShipChoices = new ArrayList<>();
        getBackToShipChoices.add("game over");
        StoryNode getBackToShip = new StoryNode(getBackToShipText, getBackToShipChoices);
        takeRiskAndSwim.children.add(getBackToShip);

        String[] aimForShoreText = {"Forget the ship, you got off of it with a goal. Gathering up all of your strength,",
                "you make your way towards the shoreline. Along the way, you are thrust against rocks, you are steered",
                "off course by malignant currents, and the waves move you back, but eventually, you make your way to",
                "the shore as if by fate. Your vision is blurry, and you can't even move your fingers. On the sands of",
                "the shore, with the water still clutching your feet, the world around you fades to black."};
        ArrayList<String> aimForShoreChoices = new ArrayList<>();
        aimForShoreChoices.add("continue");
        StoryNode aimForShore = new StoryNode(aimForShoreText, aimForShoreChoices);
        takeRiskAndSwim.children.add(aimForShore);

        String[] stayOnShipText = {"You aren't exactly as wise as Athena, but you are wise enough to know that trying to",
                "swim in Poseidon's domain is not exactly the best of ideas. At the same time, easier said than done!",
                "The waters continue to rip your ship apart and you are left with a singular plank left to cling onto.",
                "At that moment, a figure emerges from the sea. She introduces herself as Ino, offering you a veil that",
                "claims will keep you safe. It is a godsend, literally, but too many times in the past have you been",
                "deceived by gods and goddesses."};
        ArrayList<String> stayOnShipChoices = new ArrayList<>();
        stayOnShipChoices.add("take the veil");
        stayOnShipChoices.add("refuse");
        StoryNode stayOnShip = new StoryNode(stayOnShipText, stayOnShipChoices);
        struggle.children.add(stayOnShip);

        String[] takeVeilText = {"Any offer is a good offer at this point. You cast your doubts aside and take the veil",
                "from the goddess, thanking her for her help. She smiles and submerges back into the water. You wrap",
                "the veil around you, hoping that this isn't some kind of divine prank that Ino is playing on you.",
                "Luckily for you, the veil seems to be the real deal, as it protects you from Poseidon's fury. You",
                "are finally given a rest as Athena guides you to the shore. You give the veil back to Ino",
                "once you reach the shore, and you feel the fatigue catching up to you as you take a rest on the shoreline"};
        ArrayList<String> takeVeilChoices = new ArrayList<>();
        takeVeilChoices.add("continue");
        StoryNode takeVeil = new StoryNode(takeVeilText, takeVeilChoices);
        stayOnShip.children.add(takeVeil);

        String[] refuseText = {"You've been tricked one too many times by the gods, and you can't trust anyone but Athena.",
                "You rebuke Ino for what you believe to be trickery, but turns out that was not the right choice. Ino",
                "is clearly offended by your refusal and your manners, and although she doesn't inflict any curse upon",
                "you herself, she takes the veil with her as the tides around you only grow taller. You look up in pure",
                "terror as the mass of water crashes down on you, crushing you under its weight and drowning you beneath",
                "the waters."};
        ArrayList<String> refuseChoices = new ArrayList<>();
        refuseChoices.add("game over");
        StoryNode refuse = new StoryNode(refuseText, refuseChoices);
        stayOnShip.children.add(refuse);

        // initializing the events that happen with the Phaeacians
        String[] arriveScheriaText = {"Your rest is brief. Athena appears to you in a dream and tells you to keep moving",
                "forward to the protection of the forest, where you will meet a princess that will help you get back",
                "home. You continue to move inland as Athena disappears from sight. You wait patiently in the forest,",
                "but it seems that the princess might take a while to get here. You look around and notice a couple things."};
        ArrayList<String> arriveScheriaChoices = new ArrayList<>();
        arriveScheriaChoices.add("visit the spring");
        arriveScheriaChoices.add("visit the field of flowers");
        StoryNode arriveScheria = new StoryNode(arriveScheriaText, arriveScheriaChoices);
        takeVeil.children.add(arriveScheria);
        aimForShore.children.add(arriveScheria);

        String[] visitSpringText = {"Although you've been tossed around in water, you haven't been able to get a drink of",
                "the stuff in a while. You walk over to the shimmering spring, and you take a sip from the spring. With",
                "your thirst quenched, you take a look around the spring itself. Nothing too special around here."};
        ArrayList<String> visitSpringChoices = new ArrayList<>();
        visitSpringChoices.add("head back to where you were before and take a rest");
        StoryNode visitSpring = new StoryNode(visitSpringText, visitSpringChoices);
        arriveScheria.children.add(visitSpring);

        String[] visitFlowersText = {"A change of scenery would be nice. After all, you just had quite an ordeal with water.",
                "You get up and head over to the field of flowers and take a nice look around. The species of flowers",
                "on this island are quite different from your homeland, and you take a moment to appreciate the scenery.",
                "You think about taking one of the flowers, but upon second inspection, you see that the field of",
                "flowers seems to be arranged in a certain way, as if it were a shrine. Perhaps it is only by chance though."};
        ArrayList<String> visitFlowersChoices = new ArrayList<>();
        visitFlowersChoices.add("take a flower");
        visitFlowersChoices.add("leave the flowers");
        StoryNode visitFlowers = new StoryNode(visitFlowersText, visitFlowersChoices);
        arriveScheria.children.add(visitFlowers);

        String[] takeFlowerText = {"For some reason, you think that the reward outweighs the risk. You take one of the",
                "flowers and smell it and... nothing seems to happen. Seems like you are in the clear. You head back to where you",
                "were before, and everything seems to be the same except you feel that your step is a little lighter,",
                "your smile is a little wider, and everything seems a bit brighter."};
        ArrayList<String> takeFlowersChoices = new ArrayList<>();
        takeFlowersChoices.add("go back to where you came from");
        StoryNode takeFlower = new StoryNode(takeFlowerText, takeFlowersChoices);
        visitFlowers.children.add(takeFlower);

        String[] goBackText = {"With your hunger for curiosity satisfied, you head back to the original location that Athena",
                "brought you to. You realize that it's night time, and that the princess will most likely arrive next",
                "morning if anything. The best choice here is to take a nap yourself, and so you lie down on the soft",
                "dirt near the riverbank and sleep until the morning."};
        ArrayList<String> goBackChoices = new ArrayList<>();
        goBackChoices.add("sleep until the morning");
        StoryNode goBack = new StoryNode(goBackText, goBackChoices);
        visitSpring.children.add(goBack);
        visitFlowers.children.add(goBack);
        takeFlower.children.add(goBack);

        String[] forestMorningText = {"You wake up to the sound of maidens playing around in the river. You get up from",
                "your resting spot and you take a look around, and you find someone who must surely be the princess",
                "that the goddess Athena was talking about. The problem is that you are quite dirty and ruffled up",
                "from your experience in the sea, so you should probably wash up before meeting them."};
        ArrayList<String> forestMorningChoices = new ArrayList<>();
        forestMorningChoices.add("meet her as you are");
        forestMorningChoices.add("wash up and meet her");
        StoryNode forestMorning = new StoryNode(forestMorningText, forestMorningChoices);
        goBack.children.add(forestMorning);

        String[] meetUnwashedText = {"You decide that surely the great Odysseus doesn't need to bathe himself just to",
                "meet some random princess. Unfortunately, as you step foot forwards, Athena speaks directly to your",
                "mind, warning you that common etiquette is expected when meeting royalty, no matter how great you are.",
                "She promises to put a blessing upon you as bathing so that you look more handsome than ever."};
        ArrayList<String> meetUnwashedChoices = new ArrayList<>();
        meetUnwashedChoices.add("wash up and meet her");
        StoryNode meetUnwashed = new StoryNode(meetUnwashedText, meetUnwashedChoices);
        forestMorning.children.add(meetUnwashed);

        String[] meetWashedText = {"Bathing yourself is definitely the move here. As you bathe yourself away from the",
                "maidens, Athena puts a blessing on you, making you look as handsome as possible to make a good first",
                "impression on the princess of the Phaeacians. After you finish bathing, you approach her and the",
                "maidens, as Athena foretold. As you talk to each other, you can tell that Athena has clearly influenced",
                "her heart to fall in love with you. You exchange names and find out that she is Nausicaa. Wary of the",
                "implications that bringing a stranger into the city as a princess may cause, she gives you directions",
                "to the palace and how to get to Arete, the queen."};
        ArrayList<String> meetWashedChoices = new ArrayList<>();
        meetWashedChoices.add("make your way to the palace");
        StoryNode meetWashed = new StoryNode(meetWashedText, meetWashedChoices);
        meetUnwashed.children.add(meetWashed);
        forestMorning.children.add(meetWashed);

        String[] goToPalaceText = {"Following the directions that Nausicaa gave you, you make your way over to the palace.",
                "You are about 3 quarters of the way to the palace when a young girl stops you for some reason. Although",
                "you almost tell her to get out of the way, you pause, sensing a divine presence from her. She offers",
                "a safe passage to the palace, explaining that the Phaeacians, although kind, are not friendly to",
                "outsiders. Confirming your suspicions, the young girl, who is most likely Athena, veils you with fine mist",
                "which prevents people from seeing you. She eventually takes you to the palace, after which you find the",
                "queen at a festival dedicated to Poseidon. You can't help but feel some contempt for a festival",
                "dedicated towards a god who has put you through so much toil."};
        ArrayList<String> goToPalaceChoices = new ArrayList<>();
        goToPalaceChoices.add("bow before the queen");
        goToPalaceChoices.add("curse the Phaeacians and shut down the festival");
        StoryNode goToPalace = new StoryNode(goToPalaceText, goToPalaceChoices);
        meetWashed.children.add(goToPalace);

        String[] bowToQueenText = {"You put aside your pride. As Athena and Nausicaa instructed, you kneel before the queen",
                "and bow down to her as the mist veil Athena bestowed upon you fades away. The king Alcinous asks if you",
                "are a god, but you respond that you are a mortal just like them, and you explain your situation to the",
                "king and queen, who then offer you a ship back home the very next day! Things are definitely going",
                "well. You are offered a room, and you spend the night recovering from your weakness."};
        ArrayList<String> bowToQueenChoices = new ArrayList<>();
        bowToQueenChoices.add("rest and wake up the next day");
        StoryNode bowToQueen = new StoryNode(bowToQueenText, bowToQueenChoices);
        goToPalace.children.add(bowToQueen);

        String[] curseThePhaeaciansText = {"A festival to Poseidon? The very god that has made your journey back to Ithaca",
                "so painful and miserable? You can't stand the thought of these people celebrating Poseidon, and your",
                "rage boils within you as you clench your fists. You spot a guard off to the side. You can easily kill",
                "him and take his weapons, wreaking havoc and cursing the Phaeacians for their dedication to Poseidon.",
                "Just as you begin to approach the guard, your legs give out and you fall to the ground. You enter a",
                "dreamlike state as Athena speaks directly to your mind. She harshly rebukes you for your irrational",
                "thought process and is disappointed. To be blessed by the god of wisdom and be so unwise is an insult",
                "to herself, and although she promises to continue to support you, you can tell that you did not meet",
                "her expectations. You decide that it is best to go and bow to the queen after all."};
        ArrayList<String> curseThePhaeaciansChoices = new ArrayList<>();
        curseThePhaeaciansChoices.add("bow before the queen");
        StoryNode curseThePhaeacians = new StoryNode(curseThePhaeaciansText, curseThePhaeaciansChoices);
        goToPalace.children.add(curseThePhaeacians);
        curseThePhaeacians.children.add(bowToQueen);

        String[] phaeacianAssemblyText = {"You wake up the next day and you are guided to an assembly of Phaeacian counselors.",
                "King Alcinous proposes to allow you to return to your homeland via ship, and the measure is approved with",
                "no problems in the way. Your journey back home is finally coming together, and King Alcinous puts together",
                "a feast and ceremony to celebrate. During the celebration, Alcinous invites the blind bard Demodocus.",
                "You look forward to the bard's performance, and as he starts his story you listen with eager ears. Within",
                "minutes, you realize very quickly that the bard is singing the tales of you and Achilles and your great",
                "feats at the siege of Troy. Although you feel honored, you cannot suppress your emotion as you recollect",
                "the scarring memories from not only the war but the journey back home. King Alcinous notices your grief,",
                "and he calls for the bard to stop singing as he starts the athletic games."};
        ArrayList<String> phaeacianAssemblyChoices = new ArrayList<>();
        phaeacianAssemblyChoices.add("continue towards the athletic games");
        StoryNode phaeacianAssembly = new StoryNode(phaeacianAssemblyText, phaeacianAssemblyChoices);
        bowToQueen.children.add(phaeacianAssembly);

        String[] athleticsText = {"You and the others head to the athletic grounds, with the games consisting of boxing,",
                "wrestling, racing, and the discus throw. You enjoy the sports, and eventually, the other Phaeacians,",
                "noticing your constitution and build, ask you to participate to see what you've got. Although rested up,",
                "you still feel weak due to the events prior, and you aren't sure whether you really want to participate",
                "or not."};
        ArrayList<String> athleticsChoices = new ArrayList<>();
        athleticsChoices.add("show them what you've got");
        athleticsChoices.add("respectfully decline");
        StoryNode athletics = new StoryNode(athleticsText, athleticsChoices);
        phaeacianAssembly.children.add(athletics);

        String[] showThemText = {"Even in your weakened state, you feel confident in your athletic prowess. You are Odysseus",
                "after all. You get up and declare your participation in the competition and head to the center of the",
                "field as the other Phaeacian athletes look at their new challenger. The first event for you to compete",
                "in is the discus toss. Out of the corner of your eye, you notice Broadsea, one of the Phaeacian athletes,",
                "rolling his eyes at you and smirking."};
        ArrayList<String> showThemChoices = new ArrayList<>();
        showThemChoices.add("wipe that smirk off his face");
        StoryNode showThem = new StoryNode(showThemText, showThemChoices);
        athletics.children.add(showThem);

        String[] declineText = {"The past days have taken too much of a toll on your body for you to compete at the level",
                "you want to for this kind of event. You decline, but Broadsea, one of the Phaeacian athletes, goads",
                "you by claiming that you are too scared to participate. Your pride overwhelms you and you are filled",
                "with the determination to end this man's career on the spot. The first event is the discus toss. No",
                "mercy for those who taunt Odysseus."};
        ArrayList<String> declineChoices = new ArrayList<>();
        declineChoices.add("end this man's career with the discus toss");
        StoryNode decline = new StoryNode(declineText, declineChoices);
        athletics.children.add(decline);

        String[] discusText = {"With pride as your engine, you walk up to the discus toss and prepare yourself. All the",
                "other Phaeacian athletes perform their tosses, getting a distance that isn't terrible but is nowhere",
                "near impressive. Then, Broadsea takes the discus and performs his toss, beating every other athlete",
                "by a wide margin. The other Phaeacians cheer, and as you step up to perform your toss as the final",
                "competitor, eyes are locked onto you. You pick up the discus and gauge its weight before performing",
                "a magnificent toss, easily beating all the other Phaeacians and Broadsea combined. The athletes are",
                "in shambles, and you let your pride overtake you as you challenge them to any other contest, because",
                "you know that you can beat them all."};
        ArrayList<String> discusChoices = new ArrayList<>();
        discusChoices.add("continue");
        StoryNode discus = new StoryNode(discusText, discusChoices);
        showThem.children.add(discus);
        decline.children.add(discus);

        String[] defuseText = {"As the argument is getting more heated, King Alcinous steps in to stop it. He redirects",
                "everyone to one final feast before your departure, which everyone agrees to. As you are eating dinner,",
                "the Phaeacian youths perform songs and dances for you, and give gifts for you to take home as an",
                "apology for what happened earlier. You apologize to them as well for your hot-headedness. After you",
                "finish receiving the gifts, the bard again sings of the siege of Troy and the Trojan Horse, and once",
                "again, you cannot contain your emotions. King Alcinous, with his sharp eyes, notices again, stops the",
                "music, and asks for your identity and who you truly are. After recalling the events leading up to this",
                "point, from Polyphemus, to Scylla, to the Trojan War itself, to Calypso, you explain every part of your",
                "tragic attempt to return home. The Phaeacians sympathize with you and once again commit themselves to",
                "getting you a safe voyage back home. You are returned to your room, where you prepare to fall asleep"};
        ArrayList<String> defuseChoices = new ArrayList<>();
        defuseChoices.add("go to sleep");
        StoryNode defuse = new StoryNode(defuseText, defuseChoices);
        discus.children.add(defuse);

        String[] marriageOfferText = {"At least, you were going to go to sleep. A knock at your door startles you, and you",
                "open it to see that King Alcinous himself has come to make you an offer. He explains that he is very",
                "moved by your story, and is extremely impressed by not only your athletic prowess but also your wisdom",
                "and strong will against all odds. He understands your desire to get back home, but despite this, he",
                "asks if you would take his daughter's hand in marriage. You contemplate to yourself. You are most",
                "likely thought to long dead back in Ithaca, and your family and kingdom may have already moved on with",
                "a new ruler. Restarting your life here in Scheria might not be so bad, with a princess as your wife and a",
                "promising future in the ruling upper class."};
        ArrayList<String> marriageOfferChoices = new ArrayList<>();
        marriageOfferChoices.add("restart your life here in Scheria");
        marriageOfferChoices.add("deny the offer so you can get back to Ithaca");
        StoryNode marriageOffer = new StoryNode(marriageOfferText, marriageOfferChoices);
        defuse.children.add(marriageOffer);

        String[] restartLifeText = {"You accept the marriage offer, and Nausicaa and Alcinous both express joy at your",
                "decision. You go to sleep, unsure if you made the right choice or not. As you sleep, Athena appears",
                "to you in a dream, and expresses disappointment in your choice. At the same time, she understands",
                "your desire to avoid more conflict and strife in your life, given that you have experienced so many",
                "challenges already. You wake up the next day and Alcinous declares your relationship to Nausicaa to",
                "the rest of the Phaeacians. In the coming years, you are well accepted among the Phaeacians and become",
                "somewhat of a celebrity figure and a leader, as well as a trustworthy spouse to your new wife Nausicaa.",
                "Although you were not able to make it back to Ithaca, you now live a peaceful and content life in",
                "Scheria, never again to be touched by the tragedies of the war. Everything seems great, but at night,",
                "you always go through that dream with Athena again and again, and although you are happy, you can't help but wonder",
                "if you made the right choice or not."};
        ArrayList<String> restartLifeChoices = new ArrayList<>();
        restartLifeChoices.add("game over");
        StoryNode restartLife = new StoryNode(restartLifeText, restartLifeChoices);
        marriageOffer.children.add(restartLife);

        String[] denyForIthacaText = {"A lucrative new life sounds promising, but you have an overwhelming sense of duty",
                "to your kingdom. You decline the offer, communicating your devotion to return to Ithaca and reestablish",
                "your family's rightful place in the kingdom. Although Alcinous is dismayed, he completely understands",
                "the situation, as he explains that he would most likely do the same were he in your position. He assures",
                "you that the ship will be ready by tomorrow, and wishes you good night as leaves your room. Somewhere,",
                "you can feel Athena's presence, affirming that this what the right path to go down."};
        ArrayList<String> denyForIthacaChoices = new ArrayList<>();
        denyForIthacaChoices.add("go to sleep");
        StoryNode denyForIthaca = new StoryNode(denyForIthacaText, denyForIthacaChoices);
        marriageOffer.children.add(denyForIthaca);

        String[] voyageBackText = {"You wake the next morning and set sail towards Ithaca, falling asleep during the voyage",
                "as the crewmen navigate the calm waters. You drift back into sleep for the rest of the journey,",
                "eventually woken up by the crewmen letting you know that you are back in Ithaca. Overjoyed, you look",
                "out to see that you indeed near land, and the journey ends safely with you finally home with you and",
                "your gifts. You praise Athena and prepare to go back to your kingdom. As you journey inwards, you can't",
                "shake off this gut feeling that the very sailors who helped you may not have been able to get home safely",
                "the same way that you were able to, but that's a story for another time. Your journey back to Ithaca",
                "has ended, but there is still much work to do."};
        ArrayList<String> voyageBackChoices = new ArrayList<>();
        voyageBackChoices.add("journey onwards");
        StoryNode voyageBack = new StoryNode(voyageBackText, voyageBackChoices);
        denyForIthaca.children.add(voyageBack);

        // initialization for the final events leading up to the conclusion of the Odyssey
        String[] beginIthacaText = {"You get an odd feeling that Ithaca is not quite the same as it was before. In fact, is",
                "this place even Ithaca? A healthy amount of time looking around leads you to the conclusion that the",
                "Phaeacians must have just dropped you in some random place after you refused to marry their princess.",
                "As you are about to curse the Phaeacians, a shepherd meets you and assures you that you are indeed in",
                "Ithaca, after which he asks you who you are."};
        ArrayList<String> beginIthacaChoices = new ArrayList<>();
        beginIthacaChoices.add("tell him that you are Odysseus");
        beginIthacaChoices.add("conceal your identity for now");
        StoryNode beginIthaca = new StoryNode(beginIthacaText, beginIthacaChoices);
        voyageBack.children.add(beginIthaca);

        String[] tellShepherdText = {"You puff out your chest and gloat. You reveal that you are Odysseus, the king of Ithaca",
                "who has returned to claim his rightful place within the kingdom. You expect the shepherd to instantly",
                "kneel and praise the gods for your return, but instead he facepalms as his image begins to change. You",
                "realize your own foolishness and the shepherd removes his disguise to show that he, or rather, she, was",
                "actually Athena, testing your own cunning. She rebukes you for your carelessness, and makes you aware of",
                "the situation while you were gone. Ithaca is now overrun with suitors who wish to take control of the",
                "kingdom, and you have been presumed dead. You now have many enemies within the kingdom, so it is best",
                "to avoid revealing your identity to anyone. With this established, Athena gives you the appearance of",
                "an old man, and directs you towards Eumaeus, your former swineherd."};
        ArrayList<String> tellShepherdChoices = new ArrayList<>();
        tellShepherdChoices.add("trudge on towards Eumaeus");
        StoryNode tellShepherd = new StoryNode(tellShepherdText, tellShepherdChoices);
        beginIthaca.children.add(tellShepherd);

        String[] concealIdentityText = {"There's something about this shepherd that you can tell is not right. Beyond that,",
                "your wisdom tells you that you should not reveal your identity yet, and so you tell the shepherd that",
                "he must identify himself before you will do the same. The shepherd smiles as his, or rather, her, disguise",
                "fades away, revealing the shepherd to be Athena herself. She is delighted that you were able to conceal",
                "your identity and use your cunning to approach the situation, rather than gloat. She explains that this",
                "is especially important given the current state of Ithaca: it is overrun by suitors who wish to take",
                "control of the kingdom in your absence, and you have been presumed dead. You have many enemies within the",
                "kingdom, but Athena reaffirms that she has faith in your cunning and experience in war to avoid poor",
                "decisions. With this established, Athena gives you the appearance of an old man, and directs you towards",
                "Eumaeus, your former swineherd."};
        ArrayList<String> concealIdentityChoices = new ArrayList<>();
        concealIdentityChoices.add("visit Eumaeus");
        StoryNode concealIdentity = new StoryNode(concealIdentityText, concealIdentityChoices);
        beginIthaca.children.add(concealIdentity);

        String[] eumaeusText = {"Eventually, you find Eumaeus outside of his hut, as usual. Eumaeus doesn't recognize you,",
                "but he takes you in anyway, offering you a hearty meal as well. You appreciate Eumaeus' hospitality,",
                "and although you continue to conceal your identity per Athena's guidance, you assure him that he may",
                "see his master quite soon again. However, he quickly interrupts you, as he has seen one too many people",
                "attempt to get favors from Odysseus' prior servants and especially his wife, Penelope. Upon hearing your",
                "wife's name, you almost mourn, but stop yourself to prevent any further suspicion. Eumaeus offers you",
                "rest in his hut, a kind gesture that you accept. That night, Athena appears to you in a dream and lets",
                "you know that Telemachus is searching for you, motivated by a vision that she gave him of a reunion",
                "between father and son."};
        ArrayList<String> eumaeusChoices = new ArrayList<>();
        eumaeusChoices.add("continue");
        StoryNode eumaeus = new StoryNode(eumaeusText, eumaeusChoices);
        tellShepherd.children.add(eumaeus);
        concealIdentity.children.add(eumaeus);

        String[] eumaeusContinuedText = {"The next morning, you wake up with your confidence reestablished with the dream",
                "from last night. You and Eumaeus make some conversation before a visitor arrives, grabbing the attention",
                "of both of you. Eumaeus welcomes the guest in, who turns out to be Telemachus, your son. You can barely",
                "recognize him, but he certainly takes on some of your traits. You can tell he has matured significantly,",
                "and will be a vital part of your plan to take back Ithaca. Eumaeus explains your situation to Telemachus,",
                "then suggests to Telemachus that he should take you back to the palace. Fearing the suitors, Telemachus",
                "explains that he believes it is better for Eumaeus alone to go to the palace to explain the situation",
                "to Penelope, so he goes off, leaving only father and son in the hut. After Eumaeus has left, Athena removes",
                "your disguise, revealing your true identity to Telemachus. He is paralyzed in shock and cannot believe",
                "his eyes, and after taking a moment to process what happened, the two of you embrace and weep, joyous",
                "of your return to Ithaca."};
        ArrayList<String> eumaeusContinuedChoices = new ArrayList<>();
        eumaeusContinuedChoices.add("start plotting how to kill the suitors");
        StoryNode eumaeusContinued = new StoryNode(eumaeusContinuedText, eumaeusContinuedChoices);
        eumaeus.children.add(eumaeusContinued);

        // initialization of the final events
        String[] plotText = {"As things go down at the palace, you and Telemachus discuss the plot to kill the suitors.",
                "There are a couple of options available, and you and Telemachus come up with 3 main plans that you both",
                "agree could be the best courses of action. The first, a standard takeover of the palace. You and Telemachus",
                "would gather support from the citizenry with the guidance of Athena, and drive the suitors out of Ithaca.",
                "On the other hand, a less conspicuous approach is another option. Poisoning the food at one of their",
                "banquets is an easy, effective idea. The last idea is to launch a surprise attack, concealing the weapons",
                "so that the suitors never have a chance to fight back."};
        ArrayList<String> plotChoices = new ArrayList<>();
        plotChoices.add("head on assault");
        plotChoices.add("poison the food");
        plotChoices.add("use a surprise attack");
        StoryNode plot = new StoryNode(plotText, plotChoices);
        eumaeusContinued.children.add(plot);

        String[] assaultText = {"You decide that a full on assault is the best way to get the support of the people and to",
                "overthrow the suitors as efficiently and thoroughly as possible. The problem is getting the news out",
                "to the rest of the citizenry without the suitors noticing. With the suitors holding complete control",
                "over the palace for now, it won't be possible to get a message out to the people easily without the",
                "suitors noticing. You and Telemachus plot possible ways to make the plan succeed, but come up with no",
                "clear answers in the end. As you both spend the night at the hut, you pray to Athena, hoping that she",
                "will be able to aid you in your efforts. She appears, once again in your dreams, this time in the",
                "form of an owl. In your dream, you ask her on the best course of action, but the owl simply flies",
                "away, leaving behind a dead eagle. You interpret it as her conveying that the head on assault could",
                "lead to your tragic downfall, so in the morning, you decide to plot with Telemachus again on a possible",
                "alternate plan."};
        ArrayList<String> assaultChoices = new ArrayList<>();
        assaultChoices.add("rethink things over with Telemachus");
        StoryNode assault = new StoryNode(assaultText, assaultChoices);
        plot.children.add(assault);
        assault.children.add(plot);

        String[] poisonText = {"Although a bit cynical, poisoning the food is a quick and easy way to spontaneously kill",
                "all of the suitors without too many problems in the way. From what you've heard from Telemachus, the",
                "suitors have grown lax due to their constant, unchallenged occupation of the palace, so they have",
                "stopped testing for food poisoning with servants. A window of opportunity is open for this method, so",
                "you and Telemachus decide to take the chance. After resolving to take on this plan, Telemachus leaves",
                "for the palace in order to settle issues there while you search for an alchemist that create a toxin for",
                "the plan."};
        ArrayList<String> poisonChoices = new ArrayList<>();
        poisonChoices.add("search for an alchemist near the palace");
        poisonChoices.add("search for an alchemist on the outskirts of the kingdom");
        StoryNode poison = new StoryNode(poisonText, poisonChoices);
        plot.children.add(poison);

        String[] alchemistPalaceText = {"There must be an alchemist somewhere near the palace. After all, the area surrounding",
                "the palace is a populated commercial area, and there is bound to be an alchemist or two that dabbles",
                "in the illegal arts. Although you would normally shut down such places, right now, they are essential",
                "to your plan. After a bit of searching around, you finally find an alchemist that seems to be able",
                "to create the poisons you need in order to execute your plan. Paying the price in full with the gifts",
                "from the Phaeacians, you obtain the poisons needed for the plan. More importantly, you also pay the",
                "alchemist to keep his mouth shut: a vital pillar to the plan."};
        ArrayList<String> alchemistPalaceChoices = new ArrayList<>();
        alchemistPalaceChoices.add("return to the hut");
        StoryNode alchemistPalace = new StoryNode(alchemistPalaceText, alchemistPalaceChoices);
        poison.children.add(alchemistPalace);

        String[] alchemistOutskirtsText = {"It'll be difficult to find an alchemist away from the populated commercial area",
                "that is the palace's surroundings, but getting your poisons from someone on the outside means that",
                "nobody will be able to track your purchase and figure out the plan. The safer bet to keep the whole",
                "thing secret is to purchase from the outskirts. It takes you several hours in order to find even a",
                "single alchemist, but you eventually do find one. Using your gifts from the Phaeacians, you purchase",
                "the necessary poisons from the alchemist. You offer payment to keep quiet about the purchase, but she",
                "just smiles and denies the payment. As you leave her shop, you realize that she probably identified",
                "who you were, and is keeping quiet because she also despises the suitors."};
        ArrayList<String> alchemistOutskirtsChoices = new ArrayList<>();
        alchemistOutskirtsChoices.add("return to the hut");
        StoryNode alchemistOutskirts = new StoryNode(alchemistOutskirtsText, alchemistOutskirtsChoices);
        poison.children.add(alchemistOutskirts);

        String[] alchemistPalaceContinuedText = {"Veiled by Athena's disguise, you quickly move out of the city center back",
                "to Eumaeus' hut. You meet Telemachus, who tests the poison against a rat. Some strong stuff indeed: it",
                "kills the rat within seconds as a fast acting poison. During his visit to the palace, Telemachus tells",
                "you that he found out that there will be a banquet today in order for the suitors to try and win",
                "Penelope's favor again. You figure that this is the perfect chance to take back what is yours, so you",
                "and Telemachus quickly head to the palace center by dinner time. With Athena's veil, you are able to",
                "sneak into the kitchen while Telemachus distracts the suitors and their chefs, and you are able to slip",
                "the poison into their food successfully. You smile at your successes, but in the back of your mind,",
                "you can't help but feel that something is off."};
        ArrayList<String> alchemistPalaceContinuedChoices = new ArrayList<>();
        alchemistPalaceContinuedChoices.add("wait for the results of the poison");
        StoryNode alchemistPalaceContinued = new StoryNode(alchemistPalaceContinuedText, alchemistPalaceContinuedChoices);
        alchemistPalace.children.add(alchemistPalaceContinued);

        String[] alchemistPalaceFinalText = {"You wait patiently with Telemachus in the dining hall, and with your presence",
                "unnoticed, you quietly observe the events that unfold before your eyes. The banquet starts and the suitors",
                "raise a toast to Penelope, which she grudgingly accepts with contempt. The suitors begin to pick up their",
                "food and prepare to eat it, and you hold back a smile as some of the suitors begin to eat. However, in",
                "the midst of the eating, you notice one of the suitors offer Penelope some of the delicacies made from his",
                "home city-state's chef, which she accepts out of curiosity. Eyes wide with horror, you leap to your feet and",
                "shout, trying to stop her from eating the food, but she eats it anyway. You close your eyes... but Penelope seems",
                "to be fine! Your wife is okay, but unfortunately, you've just grabbed the attention of the entire room.",
                "No amount of veiled trickery from Athena is going to help you here. The suitors easily figure out that",
                "you are the one that purchased the poison from the alchemist, who gave away your plan in exchange for",
                "the larger sum of money that the suitors had. The poisoned food never made it to the table. You are",
                "captured by the suitors' personal guards, and you are executed the next day in front of the rest of the",
                "kingdom as an example."};
        ArrayList<String> alchemistPalaceFinalChoices = new ArrayList<>();
        alchemistPalaceFinalChoices.add("game over");
        StoryNode alchemistPalaceFinal = new StoryNode(alchemistPalaceFinalText, alchemistPalaceFinalChoices);
        alchemistPalaceContinued.children.add(alchemistPalaceFinal);

        String[] alchemistOutskirtsContinuedText = {"You make your way back to the hut. Telemachus is a bit doubtful about",
                "the poison given its source, but he tests it against a rat. At first, it seems to do nothing, but a",
                "couple of minutes later, the rat keels over and collapses, unable to breath. A slow acting poison, it",
                "seems. Perfect for your purposes, as it prevents anyone from realizing what's happening. During his",
                "visit to the palace, Telemachus was able to find out that there will be a banquet today for the suitors",
                "to try and win Penelope's favor. This is the perfect opportunity to capitalize on, so with the help",
                "of Athena's veil, you are able to sneak into the kitchen while Telemachus distracts the suitors and",
                "their chef. Slipping the poison into the suitors' food, you sneak away as the banquet begins, observing",
                "from the back."};
        ArrayList<String> alchemistOutskirtsContinuedChoices = new ArrayList<>();
        alchemistOutskirtsContinuedChoices.add("wait for the results of the poison");
        StoryNode alchemistOutskirtsContinued = new StoryNode(alchemistOutskirtsContinuedText, alchemistOutskirtsContinuedChoices);
        alchemistOutskirts.children.add(alchemistOutskirtsContinued);

        String[] alchemistOutskirtsFinalText = {"You wait patiently with Telemachus in the dining hall, and with your presence",
                "unnoticed, you quietly observe the events that unfold before your eyes. The banquet starts and the suitors",
                "raise a toast to Penelope, which she grudgingly accepts with contempt. The suitors begin to pick up their",
                "food and prepare to eat it, and you hold back a smile as some of the suitors begin to eat. However, in",
                "the midst of the eating, you notice one of the suitors offer Penelope some of the delicacies made from his",
                "home city-state's chef, which she accepts out of curiosity. Eyes wide with horror, you leap to your feet and shout,",
                "trying to stop her from eating the food, but she eats it anyway. You were unable to stop her and you are stunned",
                "as you realize that you will be the cause of your wife's death. Not only that, you've caught the attention",
                "of all the suitors and they begin to motion for their personal guards to capture you. By a stroke of",
                "pure chance, the poison from the alchemist starts to kick in as the suitors, one by one, are clutched",
                "by the grip of Hades. By the end of the whole ordeal, the only ones left standing in the banquet hall",
                "are you and Telemachus, horrified by the fact that the two of you are responsible for killing Penelope.",
                "Struck with grief, you contemplate taking the poison yourself, but Telemachus stops you, reminding you",
                "of your duty to the kingdom and your journey thus far. You have successfully returned to Ithaca and",
                "killed the suitors, but at the cost of your family, and your dreams will forever be haunted by this day."};
        ArrayList<String> alchemistOutskirtsFinalChoices = new ArrayList<>();
        alchemistOutskirtsFinalChoices.add("game over");
        StoryNode alchemistOutskirtsFinal = new StoryNode(alchemistOutskirtsFinalText, alchemistOutskirtsFinalChoices);
        alchemistOutskirtsContinued.children.add(alchemistOutskirtsFinal);

        // initialize the true original ending of the Odyssey
        String[] surpriseAttackText = {"A surprise attack from the inside, with your superior skills in combat, should be",
                "a successful way to get rid of the suitors once and for all. Your plan is to hide the weapons and lock",
                "the weapons storages, so that it will be a one-sided fight all the way through. After setting your",
                "plan, Telemachus leaves the hut and heads to the palace, promising not to reveal your identity to anyone.",
                "After he leaves, you set out with Eumaeus to go towards town. Once you arrive, you request if you can",
                "sit in on the banquet that the suitors are having, and they reluctantly allow you to stay.",
                "Antinous, one of the suitors, is especially frustrated with your presence, and he insults you",
                "and your appearance and poverty. When you insult him back, he strikes you with a stool, which even the",
                "other suitors reprimand him for. Swallowing your pride, you do not fight back. He will receive his",
                "due punishment in time. Penelope hears about the incident and requests for you to meet her so she",
                "can question you about Odysseus, which she hears you have some news of. Eumaeus leaves and goes back",
                "to the hut while you try to think of a way to get to Penelope without attracting the attention of the suitors."};
        ArrayList<String> surpriseAttackChoices = new ArrayList<>();
        surpriseAttackChoices.add("ponder your choices");
        StoryNode surpriseAttack = new StoryNode(surpriseAttackText, surpriseAttackChoices);
        plot.children.add(surpriseAttack);

        String[] surpriseAttackContinuedText = {"Luckily, there's not much pondering to do, as Penelope appears before",
                "the suitors in all of her glory. She is even more beautiful than you remember her to be, most likely",
                "due to Athena's blessing. She tricks the suitors by stating that the greatest men are those who try",
                "to win women's hearts through favor, not through force. The suitors continue to give her presents,",
                "and you are delighted on the inside at Penelope's cunning. You then tell the maids to help Penelope,",
                "but they insult you, after which the suitors follow and continue to insult you as well. Eurymachus",
                "derides you particularly hard, causing you to lash back with your own insults, after which he throws",
                "one of the stools at you. Luckily, it misses and Telemachus comes in to ease the tension, but",
                "on the inside, you have never been more angered as your pride swells up."};
        ArrayList<String> surpriseAttackContinuedChoices = new ArrayList<>();
        surpriseAttackContinuedChoices.add("calm yourself down and make the first moves for the plan");
        StoryNode surpriseAttackContinued = new StoryNode(surpriseAttackContinuedText, surpriseAttackContinuedChoices);
        surpriseAttack.children.add(surpriseAttackContinued);

        String[] hideWeaponsText = {"The banquet ends, and the suitors go to rest for the night. Seeing the opportunity,",
                "you and Telemachus work quickly to hide the weapons. While you continue to hide the arms, Telemachus",
                "distracts the suitors, the maids, and their servants, directing them away from the arms and telling them",
                "the weapons are being put away to prevent damage... or something. You successfully finish hiding the",
                "weapons, and Telemachus lets you know that he is going off to bed, to prepare for the fateful day",
                "tomorrow. You decide that it's probably best for you to rest up for tomorrow as well, but to your surprise,",
                "Penelope finds you and she says that she has some things to ask her unusual and sudden visitor, especially",
                "since you have claimed to have news about Odysseus."};
        ArrayList<String> hideWeaponsChoices = new ArrayList<>();
        hideWeaponsChoices.add("talk to her about... well, yourself");
        StoryNode hideWeapons = new StoryNode(hideWeaponsText, hideWeaponsChoices);
        surpriseAttackContinued.children.add(hideWeapons);

        String[] talkWithPenelopeText = {"Penelope, curious about exactly how much you know, asks you to describe Odysseus.",
                "You perfectly describe yourself to her, from your heroic feats, to your physical features, your hobbies,",
                "your athletic accomplishments, your mental fortitude and wit, and everything else that you believe defines",
                "you as the hero you are. You continue to describe yourself, but you stop, as you can see that Penelope",
                "is in tears from how well you have described her husband. In order to try and relieve her, you tell her",
                "that Odysseus is still alive, and will probably be back very soon. Despite hearing this, Penelope tells you",
                "that she has resolved herself on marrying the first of the suitors who can fire an arrow through 12 axes tomorrow,",
                "regardless of Odysseus' current status. Regardless, she is grateful to you, and offers you a place to sleep",
                "and a maid to wash your feet. You explain that you don't need a place to sleep, but you reluctantly accept",
                "the offer to wash your feet. Eurycleia, one of your former maids, washes your feet, and as she does,",
                "you suddenly realize that she will recognize the trademark scar on your feet. You jerk your feet away from",
                "her and try to hide the scar, but she sees it and bursts into tears, embracing you. However, you quickly",
                "silence her, urgently telling her that she needs to keep this a secret, lest it be your own ruin. She",
                "promises to keep the secret, and finishes washing your feet, ending the day."};
        ArrayList<String> talkWithPenelopeChoices = new ArrayList<>();
        talkWithPenelopeChoices.add("go to sleep to prepare for the fateful day");
        StoryNode talkWithPenelope = new StoryNode(talkWithPenelopeText, talkWithPenelopeChoices);
        hideWeapons.children.add(talkWithPenelope);

        String[] struggleToSleepText = {"That night, overcome with anxiety, you pray to Athena, who then reassures you that",
                "it is fate for you to overcome the suitors. She explains that you are backed by the gods, and thus, you",
                "and Telemachus will be able to overcome the many suitors present at the palace. When you wake up the next",
                "morning, you ask Zeus for a good omen, after which he sends a clap of thunder followed by the voice of",
                "one of the maids cursing the suitors. You feel better now, and you and Telemachus dress and join the",
                "suitors at the palace. Ctesippus, another one of the suitors, throw's a cow's hoof at you, laughing",
                "and mocking you even after Telemachus threatens them with death. Little do they know of their upcoming",
                "fate."};
        ArrayList<String> struggleToSleepChoices = new ArrayList<>();
        struggleToSleepChoices.add("wait patiently as Penelope arrives");
        StoryNode struggleToSleep = new StoryNode(struggleToSleepText, struggleToSleepChoices);
        talkWithPenelope.children.add(struggleToSleep);

        String[] penelopeChallengeText = {"Penelope arrives and calls for the attention of everyone present, silencing the room.",
                "She declares that the first suitor to be able to take Odysseus' bow and shoot the arrow through the holes",
                "of 12 axes will be the person that she will marry definitively. The suitors, all eager at this chance,",
                "eagerly wait for Telemachus to set up the challenge. Telemachus himself tries his hand at the bow, but",
                "cannot draw it. One by one, the suitors that have constantly mocked you and disrespected you try their",
                "hand at the challenge, but they cannot even get themselves to be able to load the arrow and draw the bow.",
                "Although it is amusing to watch all the suitors continue to fail, you urgently head over to Eumaeus and",
                "Philoetius and reaffirm their loyalty to you, Odysseus. They lock the palace doors to prevent the suitors",
                "from escaping, and they promise to join you and Telemachus in combat. You return to the suitors and see",
                "Eurymachus, disappointed as he cannot draw the bow either. You can tell he feels inferior to Odysseus",
                "since he cannot even draw the bow, and you sense that now is the time to act. You ask for the bow from",
                "Eurymachus, who mocks you again and says that you must be drunk from the wine if you think the challenge",
                "applies to you. Telemachus steps in and orders him to hand the bow to you. It has been a long time since",
                "you have gripped this bow, but its shape is familiar to you, and you easily draw it and fire an arrow",
                "seamlessly through all 12 holes of the set of axes."};
        ArrayList<String> penelopeChallengeChoices = new ArrayList<>();
        penelopeChallengeChoices.add("start the slaughter");
        StoryNode penelopeChallenge = new StoryNode(penelopeChallengeText, penelopeChallengeChoices);
        struggleToSleep.children.add(penelopeChallenge);

        String[] finaleText = {"There's no time to gloat. You quickly fire a second arrow into the throat of Antinous, and",
                "Athena removes your disguise to show you true identity: the legendary Greek hero and ruler of Ithaca,",
                "Odysseus. The suitors are struck with terror and cannot escape. They try to get you to stop, but no",
                "matter what they say, you are resolved on ending them. They have no escape as you slaughter them for",
                "their sins, one by one. The battle is close, but Athena admires your combat skills and resolve and joins",
                "in on the battle, quickly ending the fight. At the end of the fight, the palace walls are stained with",
                "blood, and the room is left with only you, Telemachus, Eumaeus, Philoetius, and the goddess Athena.",
                "You have finally done it. You have returned home and you have freed your kingdom and family from the",
                "grip of the suitors, fulfilling the prophecy of the fates. Although there is still much work to do, you",
                "revel in your success, as you know that you have once again finally established yourself as the one true",
                "king of Ithaca."};
        ArrayList<String> finaleChoices = new ArrayList<>();
        finaleChoices.add("game over");
        StoryNode finale = new StoryNode(finaleText, finaleChoices);
        penelopeChallenge.children.add(finale);

        return title;
    }
}
