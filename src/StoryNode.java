import java.util.ArrayList;

public class StoryNode {
    public String[] text;
    public ArrayList<String> choices;
    public ArrayList<StoryNode> children;

    public StoryNode(String[] text, ArrayList<String> choices) {
        this.text = text;
        this.choices = choices;
        this.children = new ArrayList<>();
    }
}
